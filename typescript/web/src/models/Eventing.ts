
type Callback = () => void

export class Eventing{

    events: { [key: string]: Callback[] } = {}

    on = (eventName: string, cb: Callback): void => {
        this.events[eventName] = this.events[eventName]
            ? [...this.events[eventName], cb]
            : [cb]
    }

    trigger = (eventName: string): void => {
        const handlers = this.events[eventName]

        if (!handlers || handlers.length === 0) {
            return
        }
        handlers.forEach(cb => cb())
    }

}